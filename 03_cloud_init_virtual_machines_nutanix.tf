resource "nutanix_virtual_machine" "master_bind9_node" {
  for_each             = var.master_bind9_node
  name                 = each.value.name
  cluster_uuid         = data.nutanix_clusters.clusters.entities.0.metadata.uuid
  num_vcpus_per_socket = 1
  num_sockets          = each.value.cpu
  memory_size_mib      = each.value.memory

  disk_list {
    disk_size_mib = each.value.disk * 1024

    data_source_reference = {
      kind = "image"
      uuid = nutanix_image.ubuntu_2204_cloud_img.id
    }
  }

  nic_list {
    subnet_uuid = data.nutanix_subnet.default.id
    ip_endpoint_list = {
      ip = each.value.network.ipv4
    }
  }

  guest_customization_cloud_init_user_data = base64encode(data.template_file.cloud_init_gitlab_runner.rendered)

  depends_on = [
    data.nutanix_clusters.clusters,
    data.nutanix_image.ubuntu_2204_cloud_img,
    data.nutanix_subnet.default,
  ]
}

# #resource "nutanix_virtual_machine" "master_loadbalancer_node" {}

# #resource "nutanix_virtual_machine" "master_control_plane_node" {}

# #resource "nutanix_virtual_machine" "additional_control_plane_nodes" {}

# #resource "nutanix_virtual_machine" "master_worker_node" {}

# #resource "nutanix_virtual_machine" "additional_worker_nodes" {}