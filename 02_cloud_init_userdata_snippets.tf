locals {
  dns_reversed_ip     = join(".", reverse(split(".", var.dns_ipaddress)))
  dns_ip_parts        = split(".", local.dns_reversed_ip)
  dns_arpa_part_value = "${local.dns_ip_parts[2]}.${local.dns_ip_parts[3]}"
  dns_last_octet      = local.dns_ip_parts[0]
  dns_arpa            = "0.${local.dns_arpa_part_value}.in-addr.arpa"

  master_ip_ha_parts   = split(".", var.k8s_master_ipaddress_ha)
  master_ha_last_octet = local.master_ip_ha_parts[3]

  kubernetes_version       = split(".", var.kubernetes_version)
  kubernetes_version_minor = "${local.kubernetes_version[0]}.${local.kubernetes_version[1]}"
}

resource "local_file" "master_bind9_node_snippet" {
  filename        = var.master_bind9_node_snippet.path
  file_permission = "666"
  content         = <<-EOT
#cloud-config
hostname: bind9-master-0
fqdn: bind9-master-0
manage_etc_hosts: true
package_upgrade: true
packages:
  - qemu-guest-agent
  - htop
  - git
  - mc
  - ca-certificates
  - curl
  - gnupg-agent
  - software-properties-common
  - open-iscsi
  - nfs-common
  - ansible
  - gnupg2 
  - apt-transport-https 
  - lsb-release
  - python3-pip
  - bind9

timezone: ${var.timezone}

users:
  - name: ${var.cloud_init_user.name}
    gecos: ${var.cloud_init_user.gecos}
    sudo: 'ALL=(ALL) NOPASSWD:ALL'
    groups: [users, admin, docker]
    passwd: ${var.cloud_init_user.password}
    shell: /bin/bash
    lock-passwd: false
    ssh_pwauth: true
    ssh_authorized_keys:
        %{~for keys in var.cloud_init_user.ssh_keys~}
        - ${keys};
        %{~endfor~}
ca_certs:
  remove_defaults: false
  trusted:
  - |
${var.trusted_certs_bundle}

write_files:
  - path: /etc/bind/rndc.key
    content: |
      key "rndc-key" {
        algorithm ${var.dns_rndc_key.tsigAlgorithm};
        secret "${var.dns_rndc_key.tsigSecret}";
      };

  - path: /etc/bind/named.conf.options
    content: |
      acl goodclients {
        %{~for ip in var.dns_acl_good_client~}
        ${ip};
        %{~endfor~}
        localhost;
        localnets;
      };

      options {
        directory "/var/cache/bind";

        recursion yes;

        allow-query { goodclients; };
        
        forwarders {
          %{~for ip in var.dns_forwarders~}
          ${ip};
          %{~endfor~}
        };

        //========================================================================
        // If BIND logs error messages about the root key being expired,
        // you will need to update your keys.  See https://www.isc.org/bind-keys
        //========================================================================
        dnssec-validation auto;

        listen-on-v6 { any; };
      };

  - path: /etc/bind/named.conf.local
    content: |
      include "/etc/bind/rndc.key";
      zone "${var.dns_zone}" {
        type master;
        file "/var/cache/bind/forward.${var.dns_zone}";
        allow-transfer { key rndc-key; };
        allow-update { key rndc-key; };
      };

      zone "${local.dns_arpa}" {
        type master;
        file "/var/cache/bind/reverse.${var.dns_zone}";
        allow-transfer { key rndc-key; };
        allow-update { key rndc-key; };
      };

  - path: /var/cache/bind/reverse.${var.dns_zone}
    content: |
      $TTL    604800
      @       IN      SOA     ns1.${var.dns_zone}. root.ns1.${var.dns_zone}. (
                                    1
                              604800
                                86400
                              2419200
                              604800 )
      @       IN      NS      ns1.${var.dns_zone}.
      ns1     IN      A       ${var.dns_ipaddress}
      ${local.dns_last_octet}      IN      PTR     ns1.${var.dns_zone}.
      ${local.master_ha_last_octet}      IN      PTR     master.${var.dns_zone}.
      
  - path: /var/cache/bind/forward.${var.dns_zone}
    content: |
      $TTL    604800
      @       IN      SOA     ns1.${var.dns_zone}. root.ns1.${var.dns_zone}. (
                                    2         ; Serial
                              604800         ; Refresh
                                86400         ; Retry
                              2419200         ; Expire
                              604800 )       ; Negative Cache TTL
      @       IN      NS      ns1.${var.dns_zone}.
      ns1     IN      A       ${var.dns_ipaddress}
      master  IN      A       ${var.k8s_master_ipaddress_ha}
      @       IN      AAAA    ::1

runcmd:
  - |
    apt update
    apt install -y qemu-guest-agent
    systemctl enable qemu-guest-agent 
    systemctl start qemu-guest-agent
    systemctl restart bind9

power_state:
  delay: now
  mode: reboot
  message: Rebooting after cloud-init completion
  condition: true   
  EOT
}