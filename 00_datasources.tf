data "nutanix_clusters" "clusters" {}

data "nutanix_image" "ubuntu_2204_cloud_img" {
  image_name = nutanix_image.ubuntu_2204_cloud_img.name

  depends_on = [
    nutanix_image.ubuntu_2204_cloud_img
  ]
}

data "nutanix_subnet" "default" {
  subnet_name = var.nutanix_subnet

  depends_on = [
    data.nutanix_clusters.clusters,
  ]
}