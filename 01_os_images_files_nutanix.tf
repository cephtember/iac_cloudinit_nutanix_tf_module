resource "nutanix_image" "ubuntu_2204_cloud_img" {
  name        = var.ubuntu_2204_cloud_image.name
  source_uri  = var.ubuntu_2204_cloud_image.image_link
  description = var.ubuntu_2204_cloud_image.description
}